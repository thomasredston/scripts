#!/usr/bin/env python3
# Plot UK EPP traffic, grouped by command

import re
from collections import defaultdict
import loghost
import plot
import datetime
import os


output_dir = '/tmp/uk-epp-performance'


def parse_date(day, time):
	'''
	With 1hr resolution
	'''
	return datetime.datetime(int(day[0:4]), int(day[4:6]), int(day[6:8]), int(time[0:2]))


class EppCommandTypeParser(loghost.Parser):
	def parse(self, logfile):
		day = loghost.get_date_from_logfile(logfile)
		logline = re.compile(r'(?P<time>\d{2}:\d{2}:\d{2}) [\S]+ \([^,]+, \d+, \d+\) Action: (?P<message>.*)$')
		lines = (logline.search(l) for l in loghost.open_logfile(logfile) if ') Action: ' in l)
		return ((parse_date(day, l.group('time')), loghost.remap_uk_message(l.group('message'))) for l in lines if l is not None)


def count_epp_command_types(logfiles):
	command_counts = loghost.get_counts_from_logfiles(logfiles, EppCommandTypeParser(), loghost.classify_commands)

	total_command_counts = {'query': defaultdict(int), 'transform': defaultdict(int), 'total': defaultdict(int)}
	for op,v in command_counts.items():
		for h,c in v.items():
			total_command_counts['total'][h] += c
			if op in loghost.uk_epp_query_commands:
				total_command_counts['query'][h] += c
			if op in loghost.uk_epp_transform_commands:
				total_command_counts['transform'][h] += c

	for k,v in command_counts.items():
		s = sum(v.values())
		print(f'{k}: {s}')

	grouping = []
	for o in loghost.uk_epp_object_types:
		grouping.append([f'{o} {t}' for t in loghost.uk_epp_query_types])
		grouping.append([f'{o} {t}' for t in loghost.uk_epp_transform_types])

	grouped = [{k:v for k, v in command_counts.items() if any(c in k for c in group)} for group in grouping]
	grouped += [
		{k:v for k, v in command_counts.items() if not any(c in k for c in [command for group in grouping for command in group])},
		total_command_counts,
	]
	return grouped


def main_plot_epp_traffic(env, days):
	title = f'UK EPP Traffic'
	print(f'Starting {title}')
	logfiles = []
	for day in days:
		logfiles += loghost.get_logfiles(env, 'ravao_epp', day)

	grouped = count_epp_command_types(logfiles)
	print(f'Done {title}')

	if not os.path.exists(output_dir):
		os.makedirs(output_dir)

	plot.plot_commands_by_type(title, grouped, f'{output_dir}/{days[0]}-{days[-1]}-uk-epp-traffic.png', datetime.timedelta(hours=1))

if __name__== "__main__":
	start = datetime.date(2022, 5, 4)
	days = [start + datetime.timedelta(days = x) for x in range(1)]

	main_plot_epp_traffic(loghost.Environment(loghost.Platform.UK, loghost.Deployment.REGISTRY, loghost.Lifecycle.PRODUCTION), days)
