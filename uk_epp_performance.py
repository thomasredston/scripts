#!/usr/bin/env python3
# Plot UK EPP traffic, grouped by command

import re
from collections import defaultdict
import loghost
import plot
import datetime
import multiprocessing
from gtld_epp_performance import print_raw_stats
import os


output_dir = '/tmp/uk-epp-performance'


def parse_date(day, time):
	'''
	With 1hr resolution
	'''
	return datetime.datetime(int(day[0:4]), int(day[4:6]), int(day[6:8]), int(time[0:2]))


def parse(logfile):
	transactions = defaultdict(list)
	try:
		day = loghost.get_date_from_logfile(logfile)
		logline = re.compile(r'(?P<time>\d{2}:\d{2}:\d{2}) [\S]+ \([^,]+, \d+, (?P<transaction>\d+)\) (?P<message>.*)$')
		lines = (logline.search(l) for l in loghost.open_logfile(logfile) if ('Action: ' in l or 'Request took ' in l))
		lines = (l for l in lines if l is not None)
		for l in lines:
			transactions[l.group('transaction')].append((parse_date(day, l.group('time')), loghost.remap_uk_message(l.group('message'))))
	except Exception as e:
		print(f'Exception processing {logfile}:')
		print(e)
	return transactions


def classify(transactions):
	duration_matcher = re.compile(r'^Request took (?P<duration>[0-9.e-]+)s to process.$')
	durations = defaultdict(lambda: defaultdict(list))
	for _,lines in transactions.items():
		command = None
		for l in lines:
			if 'Action: ' in l[1]:
				command = l[1][8:].strip()
			elif 'Request took ' in l[1]:
				if command is not None:
					m = duration_matcher.search(l[1])
					durations[command][l[0]].append(float(m.group('duration')) * 1000)
	return durations


def process_logfile(logfile):
	print(f'{logfile} processing... ')
	durations = classify(parse(logfile))
	print(f'{logfile} done')
	return loghost.flatten_defaultdict(durations)


def process_logfiles(logfiles):
	results = []
	with multiprocessing.Pool() as pool:
		results = pool.starmap(process_logfile, [(l,) for l in logfiles])

	merged = defaultdict(lambda: defaultdict(list))
	for r in results:
		for op, c in r.items():
			for h, hc in c.items():
				merged[op][h] += hc

	return merged


def plot_slow_commands_over_time(durations, days):
	slow_durations = defaultdict(lambda: defaultdict(int))
	total_slow_durations = {'queries': defaultdict(int), 'transforms': defaultdict(int), 'total': defaultdict(int)}
	for op,v in durations.items():
		for h,d in v.items():
			slow = sum([1 for i in d if i > 300])
			slow_durations[op][h] = slow
			total_slow_durations['total'][h] += slow
			if op in loghost.uk_epp_query_commands:
				total_slow_durations['queries'][h] += slow
			elif op in loghost.uk_epp_transform_commands:
				total_slow_durations['transforms'][h] += slow


	grouping = []
	for o in loghost.uk_epp_object_types:
		grouping.append([f'{o} {t}' for t in loghost.uk_epp_query_types])
		grouping.append([f'{o} {t}' for t in loghost.uk_epp_transform_types])
	grouped = [{k:v for k, v in slow_durations.items() if any(c in k for c in group)} for group in grouping]
	grouped += [
		{k:v for k, v in slow_durations.items() if not any(c in k for c in [command for group in grouping for command in group])},
		total_slow_durations,
	]

	plot.plot_commands_by_type('UK EPP Commands >300ms', grouped, f'{output_dir}/{days[0]}-{days[-1]}-uk-epp-slow-commands.png', datetime.timedelta(hours=1))


def write_epp_stats(durations):
	all_durations = defaultdict(list)
	for op,v in durations.items():
		for h,d in v.items():
			all_durations[op] += d

	print_raw_stats(all_durations, 300, loghost.uk_epp_query_commands, loghost.uk_epp_transform_commands)

	plot_slow_commands_over_time(durations, days)
	plot.plot_durations(f'{days}', {'domain create': all_durations['domain create']}, f'{output_dir}/{days[0]}-{days[-1]}-domain create.png')


def main_plot_epp_traffic(env: loghost.Environment, days):
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)

	logfiles = []
	for day in days:
		logfiles += loghost.get_logfiles(env, 'ravao_epp', day)

	durations = process_logfiles(logfiles)
	write_epp_stats(durations)


if __name__== "__main__":
	start = datetime.date(2022, 5, 4)
	days = [start + datetime.timedelta(days = x) for x in range(1)]

	main_plot_epp_traffic(loghost.Environment(loghost.Platform.UK, loghost.Deployment.REGISTRY, loghost.Lifecycle.PRODUCTION), days)
