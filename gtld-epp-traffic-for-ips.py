#!/usr/bin/env python3

import re
from collections import defaultdict
import matplotlib.pyplot as plt
import loghost


def subplot_queries_by_type(command_counts):
	for command, query_times in command_counts.items():
		x = range(0, 24)
		y = [query_times[h] for h in x]
		plt.plot(x, y, label=command)
	plt.xlabel('Hour')
	plt.ylabel('Count')
	plt.legend(loc='best')


def main_graph_epp_traffic_for_ips():
	'''
	Graph of traffic/activity (NEW, IN, OUT, END) for IPs by hour
	'''

	server = 'gtdeppxlp'
	days = ['20200520']
	# Load balancer health-check IPs
	ips = ['192.168.53.3', '192.168.53.4', 'fd2b:e5a0:6862:5:0:0:0:3', 'fd2b:e5a0:6862:5:0:0:0:4']
	logline = re.compile(loghost.gtld_epp_logline_base + r'(?P<type>[A-Z]+) \(\/(?P<ip>' + '|'.join(ips) + r'), (?:[A-Za-z0-9-]+|<unknown>), [YN], [0-9-]+-[0-9]+\)')

	for day in days:
		logfiles = loghost.get_logfiles(day, server, 'app-epp')

		commands_type = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
		for i in range(0, 4):
			lines = loghost.parse(logfiles[i], logline)
			print('Analysing... ', end='', flush=True)
			for l in lines:
				hour = loghost.get_hour_of_day(l.group('time'))
				commands_type[l.group('ip')][l.group('type')][hour] += 1
			print('Done')

		title = f'{day} gtld EPP Activity for IPs'
		print('Plotting...', end='', flush=True)
		plt.figure(figsize=[16, 9])
		plt.suptitle(title)
		for i in range(0, len(ips)):
			plt.subplot(221 + i)
			ip = ips[i]
			plt.title(ip)
			subplot_queries_by_type(commands_type[ip])
		plt.savefig(f'/tmp/{title}.png')

		print('Done')

	plt.show()


if __name__== '__main__':
	main_graph_epp_traffic_for_ips()
