#!/usr/bin/env python3

import re
import matplotlib.pyplot as plt
import numpy

def running_mean(x, N):
    cumsum = numpy.cumsum(numpy.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / float(N)

def plot(title, matches):
    window = 30
    padding = [0] * int(window / 2)

    plt.figure(figsize=[16, 9])
    plt.suptitle(title)
    plt.subplot(221)
    plt.title("Sent")
    plt.xlabel('Time (s)')
    plt.ylabel('Records')
    plt.grid(axis='y')
    plt.plot([float(m.group(1)) * 1000 for m in matches], linestyle='', marker='.', markersize=2)
    plt.plot(running_mean(padding + [float(m.group(1)) * 1000 for m in matches], window))

    plt.subplot(222)
    plt.title("Failed")
    plt.xlabel('Time (s)')
    plt.ylabel('Records')
    plt.plot([float(m.group(2)) * 1000 for m in matches])

    plt.subplot(223)
    plt.title("Pending")
    plt.xlabel('Time (s)')
    plt.ylabel('Records')
    plt.grid(axis='y')
    plt.plot([float(m.group(3)) for m in matches], linestyle='', marker='.', markersize=2)
    plt.plot(running_mean(padding + [float(m.group(3)) for m in matches], window))

    plt.subplot(224)
    plt.title("Dropped")
    plt.xlabel('Time (s)')
    plt.ylabel('Records')
    plt.plot([float(m.group(4)) * 1000 for m in matches])

def parse(inputFile):
    r = re.compile(r"Sent: ([0-9\.]+)Kr/s; Failed: ([0-9\.]+)Kr/s; Pending: ([0-9\.]+); Dropped: ([0-9\.]+)Kr/s;")

    matches = [r.search(line) for line in open(inputFile, "r")]
    matches = matches[::2]
    matches = [m for m in matches if m is not None]
    return matches

def trim(matches):
    while (float(matches[0].group(1)) < 0.1):
        del matches[0]
    while (float(matches[len(matches) - 1].group(1)) < 0.1):
        del matches[len(matches) - 1]
    return matches

# matches = trim(parse("service-sep-dnstap-streamer_console.20190607.log"))
# plot("40KQ/s", trim(matches[:3000]))
# plot("45KQ/s", trim(matches[3000:]))

# matches = trim(parse("service-sep-dnstap-streamer_console.20190610.log"))
# plot("46KQ/s", trim(matches))

b1 = 510
b2 = 1000
b3 = 1750

matches = trim(parse("service-sep-dnstap-streamer_console.20190709-1.log"))
plot("46KQ/s server 1", trim(matches[:b1]))
matches = trim(parse("service-sep-dnstap-streamer_console.20190709-2.log"))
plot("46KQ/s server 2", trim(matches[:b1]))

matches = trim(parse("service-sep-dnstap-streamer_console.20190709-1.log"))
plot("25KQ/s server 1", trim(matches[b1:b2]))
matches = trim(parse("service-sep-dnstap-streamer_console.20190709-2.log"))
plot("25KQ/s server 2", trim(matches[b1:b2]))

matches = trim(parse("service-sep-dnstap-streamer_console.20190709-1.log"))
plot("23KQ/s server 1", trim(matches[b2:b3]))
matches = trim(parse("service-sep-dnstap-streamer_console.20190709-2.log"))
plot("23KQ/s server 2", trim(matches[b2:b3]))

matches = trim(parse("service-sep-dnstap-streamer_console.20190709-1.log"))
plot("24KQ/s server 1", trim(matches[b3:]))
matches = trim(parse("service-sep-dnstap-streamer_console.20190709-2.log"))
plot("24KQ/s server 2", trim(matches[b3:]))

plt.show()
