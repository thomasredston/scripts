#!/usr/bin/env python3
# Plot gTLD whois timing logs

import re
import loghost
import datetime
import os
import itertools
import plot

output_dir = '/tmp/gtld-whois-timing'


def parse_date(day, time):
	"""
	Turn logline time (eg May 27 00:59:59.810) as date with 10min resolution.
	"""
	return datetime.datetime(day[0:3], loghost.months[time[0:3]], int(time[4:6]), int(time[7:9]), int(time[10:11]) * 10)


def parse(logfile):
	queries = []
	try:
		day = loghost.get_date_from_logfile(logfile)
		logline = re.compile(loghost.time + r' \[GTLD=[a-z0-9-]*\]\[.*\] DEBUG WHOIS_TIMING_LOGGER - messageReceived-overalltime (?P<domain>\S*) (?P<duration>\d*)$')
		lines = (logline.search(l) for l in loghost.open_logfile(logfile))
		lines = (l for l in lines if l is not None)
		for l in lines:
			queries.append((l.group('domain'), l.group('duration')))
	except Exception as e:
		print(f'Exception processing {logfile}:')
		print(e)
	return queries


def process_logfile(logfile: str) -> list:
	print(f'{logfile} processing... ')
	durations = parse(logfile)
	print(f'{logfile} done')
	return durations


def reduce_results(results: list):
	return itertools.chain.from_iterable(results)


def main_plot_whois_timing(env: loghost.Environment, days):
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)

	logfiles = []
	for day in days:
		logfiles += loghost.get_logfiles(env, 'app-whois-timing', day)

	durations = loghost.map_reduce(logfiles, process_logfile, reduce_results)
	durations = [int(d[1]) for d in durations]
	slow = [d for d in durations if d >= 200]
	print(f'Queries: {len(durations)} Slow: {len(slow)} ({(len(slow)/len(durations)) * 100:.2f}%)')
	plot.plot_durations(f'{days}', {'whois': durations}, f'{output_dir}/{days[0]}-{days[-1]}.png')


if __name__== "__main__":
	start = datetime.date(2023, 6, 17)
	days = [start + datetime.timedelta(days = x) for x in range(1)]

	main_plot_whois_timing(loghost.Environment(loghost.Platform.GTLD, loghost.Deployment.REGISTRY, loghost.Lifecycle.PRODUCTION), days)
