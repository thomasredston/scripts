import subprocess
import re
import os
import datetime
import paramiko
import scp
import gzip
import getpass
import progressbar
import multiprocessing
from collections import defaultdict
from enum import Enum, auto


time = r'(?P<time>(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \d{2} \d{2}:\d{2}:\d{2}\.?\d*)'
rhel7_time = r'(?P<time>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})\+00:00'
gtld_epp_logline_base = time + r' \[GTLD=[a-z0-9-]*\]\[[A-Za-z0-9-]+\] [A-Z]+\s\s?u.n.gtld.epp.server.EppLoggerImpl - '

query_commands = ['domain:info', 'domain:check', 'contact:info', 'contact:check', 'host:info', 'host:check', '<poll']
transform_commands = [
	'domain:create', 'domain:update', 'domain:delete', 'domain:renew', 'domain:transfer',
	'contact:create', 'contact:update', 'contact:delete',
	'host:create', 'host:update', 'host:delete'
]
session_commands = ['<login', '<logout', '<hello', 'health-check']
epp_commands = query_commands + transform_commands + session_commands


gtld_epp_query_types = ['query', 'check', 'info']
gtld_epp_transform_types = ['create', 'update', 'delete', 'renew', 'transfer']
gtld_epp_object_types = ['domain', 'contact', 'host']
gtld_epp_query_commands = query_commands
gtld_epp_transform_commands = transform_commands


uk_epp_query_types = ['info', 'check', 'validate', 'list']
uk_epp_transform_types = ['create', 'release', 'update', 'renew', 'delete', 'unrenew', 'lock', 'unlock']
uk_epp_object_types = ['domain', 'contact', 'host', 'reseller']
uk_epp_query_commands = []
uk_epp_transform_commands = []
for o in uk_epp_object_types:
	uk_epp_query_commands += [f'{o} {t}' for t in uk_epp_query_types]
	uk_epp_transform_commands += [f'{o} {t}' for t in uk_epp_transform_types]


def remap_uk_message(message):
	return message.replace('account', 'contact').replace('query', 'info').replace('modify', 'update').replace('nameserver', 'host').replace('contact validate', 'contact update').replace('domain request', 'domain create')


local_log_dir = '/var/log/loghost'

months = {
	'Jan': 1,
	'Feb': 2,
	'Mar': 3,
	'Apr': 4,
	'May': 5,
	'Jun': 6,
	'Jul': 7,
	'Aug': 8,
	'Sep': 9,
	'Oct': 10,
	'Nov': 11,
	'Dec': 12,
}

class Credentials:
	def __init__(self):
		self.password = None

	def getuser(self):
		return 'thomas.redston'

	def getpass(self):
		if self.password is None:
			self.password = getpass.getpass()
		return self.password
credentials = Credentials()


def connect(server):
	client = paramiko.SSHClient()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(server, 22, credentials.getuser(), credentials.getpass())
	return client


def fetch_logfile(remote, local, logserver):
	print(f'Fetching {logserver}:{remote} to {local}... ')
	client = connect(logserver)
	found = False
	pbar = progressbar.DataTransferBar()
	def progress(filename, size, sent):
		pbar.max_value = size
		pbar.update(value=sent)
	with scp.SCPClient(client.get_transport(), progress=progress) as scpclient:
		try:
			scpclient.get(remote, local)
			found = True
			pbar.finish()
		except scp.SCPException as e:
			print(f'Failed: {e}')
			if 'No such file or directory' not in str(e):
				raise e
		finally:
			if not found and os.path.exists(local):
				os.remove(local)
			client.close()
	return local if found else None


def compress_logfile(logserver, from_file, to_file):
	print(f'Compressing {logserver}:{from_file} to {to_file}...')
	client = connect(logserver)
	_, stdout, stderr = client.exec_command(f'gzip -c {from_file} > {to_file}')
	stdout.channel.recv_exit_status()
	for l in stdout.readlines():
		print(l)
	stderr.channel.recv_exit_status()
	for l in stderr.readlines():
		print(l)
	client.close()
	return to_file


class Platform(Enum):
	UK = auto()
	GTLD = auto()

class Deployment(Enum):
	REGISTRY = auto()
	OTE = auto()
	TESTBED = auto()

class Lifecycle(Enum):
	PRODUCTION = auto()
	BETA1 = auto()
	BETA2 = auto()

class Environment:
	def __init__(self, platform: Platform, deployment: Deployment, stage: Lifecycle):
		self.platform = platform
		self.deployment = deployment
		self.stage = stage

	def get_log_host(self):
		loghost = ''
		if self.platform == Platform.UK:
			loghost = 'ukx-loghost7.localnet'
		elif self.platform == Platform.GTLD:
			loghost = 'gtld-loghost7.localnet'
		if self.stage == Lifecycle.BETA1 or self.stage == Lifecycle.BETA2:
			loghost = 'beta-' + loghost
		return loghost

	def get_host_pattern(self):
		pattern = ''
		if self.platform == Platform.UK:
			pattern += 'ukx'
		elif self.platform == Platform.GTLD:
			pattern += 'gtd'

		pattern += 'kw*'

		if self.platform == Platform.GTLD:
			if self.deployment == Deployment.REGISTRY:
				pattern += 'r'
			elif self.deployment == Deployment.OTE:
				pattern += 'o'
			elif self.deployment == Deployment.TESTBED:
				pattern += 't'

		pattern += 'l'

		if self.stage == Lifecycle.PRODUCTION:
			pattern += 'p*'
		elif self.stage == Lifecycle.BETA1:
			pattern += 'b[0-1][0-9]'
		elif self.stage == Lifecycle.BETA2:
			pattern += 'b[2-3][0-9]'
		return pattern


def get_logfiles(env: Environment, app: str, day: datetime.date) -> list[str]:
	'''
	Download app log files from loghost.
	Return list of full paths to local files.
	'''
	loghost = env.get_log_host()
	host_pattern = env.get_host_pattern()

	deployment = ''
	if env.platform == Platform.UK:
		if env.deployment == Deployment.OTE:
			deployment = 'ote-'
		elif env.deployment == Deployment.TESTBED:
			deployment = 'testbed-'

	remote_paths = []

	online = len(credentials.getpass()) > 0
	if online:
		client = connect(loghost)
		_, stdout, stderr = client.exec_command(f'ls /data/logs/{host_pattern}/{deployment}{app}.{day.strftime("%Y%m%d")}.log*')
		for l in stderr.readlines():
			print(l)
		client.close()
		remote_paths = list(stdout.readlines())

		logfiles = []
		for remote_path in remote_paths:
			path_components = remote_path.strip().split('/')
			host = path_components[3]
			filename = path_components[4]
			remote_path = remote_path.strip()
			local_archive = f'{local_log_dir}/{host}-{filename}'
			local_tmp = f'/tmp/{host}-{filename}.gz'

			logfile = None
			if os.path.exists(local_archive):
				logfile = local_archive
			elif os.path.exists(local_tmp):
				logfile = local_tmp
			elif remote_path.endswith('.log.gz'):
				logfile = fetch_logfile(remote_path, local_archive, loghost)
				pass
			elif remote_path.endswith('.log'):
				remote_path = compress_logfile(loghost, remote_path, f'/tmp/{host}-{filename}.gz')
				logfile = fetch_logfile(remote_path, local_tmp, loghost)
				pass
			if logfile is not None:
				logfiles.append(logfile)
		return logfiles
	else:
		result = subprocess.run(f'ls {local_log_dir}/{host_pattern}-{deployment}{app}.{day.strftime("%Y%m%d")}.log*', shell=True, capture_output=True, text=True)
		print(result.stderr)
		return result.stdout.splitlines()


def open_logfile(logfile):
	_, ext = os.path.splitext(logfile)
	if ext == '.gz':
		return gzip.open(logfile, 'rt', encoding='latin-1')
	return open(logfile, 'r')


@DeprecationWarning
def parse(logfile, matcher):
	print(f'Parsing {logfile}... ', end='', flush=True)
	commands = []
	with open_logfile(logfile) as f:
		for l in f:
			m = matcher.search(l)
			if m is not None:
				commands.append(m)
	print('Done')
	return commands


def parse_time(time):
    return datetime.datetime.strptime(time, '%b %d %H:%M:%S.%f')


def get_hour_of_day(date_string):
	return parse_time(date_string).hour


def get_date_from_logfile(logfile) -> str:
	matcher = re.compile(r'.(?P<date>\d{8}).log')
	return matcher.search(logfile).group('date')


class Parser:
	def parse(self, logfile):
		pass


@DeprecationWarning
def classify_commands(commands, command_counts):
	for hour, message in commands:
		found = False
		for op in command_counts.keys():
			if op in message:
				try:
					command_counts[op][hour] += 1
					found = True
					break
				except Exception:
					found = True
					break
		if not found:
			try:
				if 'unknown' in command_counts.keys():
					command_counts['unknown'][hour] += 1
				else:
					command_counts[message][hour] += 1
			except Exception:
					pass


@DeprecationWarning
def sum_commands(commands, command_counts):
	for hour, message in commands:
		command_counts['sent'][hour] += int(message)


@DeprecationWarning
def newest_commands(commands, command_counts):
	for hour, message in commands:
		command_counts['sent'][hour] = int(message)


def flatten_defaultdict(dd):
	'''
	defaultdict(defaultdict) -> {op:{hour:count}}
	'''
	flattened = {}
	for ip, c in dd.items():
		flattened[ip] = {}
		for h, hc in c.items():
			flattened[ip][h] = hc
	return flattened


@DeprecationWarning
def get_counts_from_logfile(logfile: str, parser: Parser, classifier):
	print(f'{logfile} processing... ')
	commands = parser.parse(logfile)
	command_counts = defaultdict(lambda: defaultdict(int))
	classifier(commands, command_counts)

	print(f'{logfile} done')
	return flatten_defaultdict(command_counts)


@DeprecationWarning
def get_counts_from_logfiles(logfiles: list[str], parser: Parser, classifier):
	results = []
	with multiprocessing.Pool() as pool:
		results = pool.starmap(get_counts_from_logfile, [(l, parser, classifier) for l in logfiles])

	all_command_counts = defaultdict(lambda: defaultdict(int))
	for r in results:
		for op, c in r.items():
			for h, hc in c.items():
				all_command_counts[op][h] += hc

	return all_command_counts


def map_reduce(logfiles: list[str], map: callable, reduce: callable):
	results = []
	with multiprocessing.Pool() as pool:
		results = pool.starmap(map, [(l,) for l in logfiles])

	return reduce(results)
