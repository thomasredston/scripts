#!/usr/bin/env python3
# Plot EPP traffic, grouped by command

import re
from collections import defaultdict
import loghost
from loghost import query_commands, transform_commands
import datetime
import plot
import itertools


def get_time(time: str):
	"""
	Turn logline time (eg May 27 00:59:59.810) as date with 10min resolution.
	"""
	# TODO year
	return datetime.datetime(2022, loghost.months[time[0:3]], int(time[4:6]), int(time[7:9]), int(time[10:11]) * 10)


class EppCommandTypeParser(loghost.Parser):
	def parse(self, logfile):
		logline = re.compile(loghost.gtld_epp_logline_base + r'IN \(\/[a-z0-9.:]+, (?:[A-Za-z0-9-]+|<unknown>), (?:Y|N), [0-9-]+-[0-9]+\) : (?P<message>.*)$')
		commands = (logline.search(l) for l in loghost.open_logfile(logfile) if ' IN ' in l)
		commands = ((get_time(l.group('time')), l.group('message')) for l in commands if l is not None)
		return commands


class EppDirectionParser(loghost.Parser):
	def parse(self, logfile):
		logline = re.compile(loghost.gtld_epp_logline_base + r'(?P<direction>[A-Z_]+)')
		commands = (logline.search(l) for l in loghost.open_logfile(logfile))
		commands = ((get_time(l.group('time')), l.group('direction')) for l in commands if l is not None)
		return commands


class EppSessionLimitIpsParser(loghost.Parser):
	def parse(self, logfile):
		logline = re.compile(loghost.gtld_epp_logline_base + r'BLE \(\/(?P<ip>[a-z0-9.:]+), (?:[A-Za-z0-9-]+|<unknown>), (?:Y|N), [0-9-]+-[0-9]+\) : eppresult=SESSION_LIMIT_EXCEEDED_CLOSING_CONNECTION,')
		commands = (logline.search(l) for l in loghost.open_logfile(logfile) if 'SESSION_LIMIT_EXCEEDED_CLOSING_CONNECTION' in l)
		commands = ((get_time(l.group('time')), l.group('ip')) for l in commands if l is not None)
		return commands


class EppSessionsToTerminateParser(loghost.Parser):
	def parse(self, logfile):
		logline = re.compile(loghost.time + r' \[GTLD=\]\[[A-Za-z0-9-]+\] INFO  u.n.g.epp.server.EppSessionManager - Sessions to kill size (?P<sessions>.*)$')
		commands = (logline.search(l) for l in loghost.open_logfile(logfile) if ' INFO ' in l)
		commands = ((get_time(l.group('time')), l.group('sessions')) for l in commands if l is not None)
		return commands


class EppExceptionParser(loghost.Parser):
	def parse(self, logfile):
		logline_rhel7 = re.compile(loghost.rhel7_time + r' gtd[a-z]{4}l(?:b|p)\d\d gtld-epp_console: (?P<error>.*)$')
		logline = re.compile(loghost.time + r' gtd[a-z]{4}l(?:b|p)\d\d gtld-epp_console: (?P<error>.*)$')
		commands_rhel7 = (logline_rhel7.search(l) for l in loghost.open_logfile(logfile) if 'gtld-epp_console: java.' in l)
		commands_rhel7 = ((datetime.datetime(int(l.group('time')[0:4]), int(l.group('time')[5:7]), int(l.group('time')[8:10]), int(l.group('time')[11:13]), int(l.group('time')[14:15]) * 10), l.group('error')) for l in commands_rhel7 if l is not None)
		commands = (logline.search(l) for l in loghost.open_logfile(logfile) if 'gtld-epp_console: java.' in l)
		commands = ((get_time(l.group('time')), l.group('error')) for l in commands if l is not None)
		return itertools.chain(commands, commands_rhel7)


class EppTotalSessionsParser(loghost.Parser):
	def parse(self, logfile):
		logline = re.compile(loghost.gtld_epp_logline_base + r'NEW \(\/[a-z0-9.:]+, <unknown>, N, [0-9-]+-[0-9]+\) : \d+\((?P<logged_in>\d+)\) sessions open')
		commands = (logline.search(l) for l in loghost.open_logfile(logfile) if ' NEW ' in l)
		commands = ((get_time(l.group('time')), l.group('logged_in')) for l in commands if l is not None)
		return commands


class EppLoggedInSessionsParser(loghost.Parser):
	def parse(self, logfile):
		logline = re.compile(loghost.gtld_epp_logline_base + r'NEW \(\/[a-z0-9.:]+, <unknown>, N, [0-9-]+-[0-9]+\) : (?P<sessions>\d+)\(\d+\) sessions open')
		commands = (logline.search(l) for l in loghost.open_logfile(logfile) if ' NEW ' in l)
		commands = ((get_time(l.group('time')), l.group('sessions')) for l in commands if l is not None)
		return commands


class MwSessionsToTerminateParser(loghost.Parser):
	def __init__(self, epp_instance):
		self._epp_instance = epp_instance
	def parse(self, logfile):
		logline = re.compile(loghost.time + r' \[GTLD=[a-z0-9-]*\]\[[A-Za-z0-9-]+\] INFO  u.n.g.m.s.s.e.EppSessionServiceImpl - Enter terminateConnectionsForEppHost for hostName: gtdeppxlp' + self._epp_instance + r' with (?P<sessions>\d+) sessionIds$')
		commands = (logline.search(l) for l in loghost.open_logfile(logfile) if ' INFO ' in l)
		commands = ((get_time(l.group('time')), l.group('sessions')) for l in commands if l is not None)
		return commands


class MwSessionsTerminatedParser(loghost.Parser):
	def __init__(self, epp_instance):
		self._epp_instance = epp_instance
	def parse(self, logfile):
		logline = re.compile(loghost.time + r' \[GTLD=[a-z0-9-]*\]\[[A-Za-z0-9-]+\] INFO  u.n.g.c.d.i.EppRegistrarConnectionDaoImpl - Exit bulkDeleteSpecifiedConnectionsForEppHost\(gtdeppxlp' + self._epp_instance + r'\) with (?P<sessions>\d+) rows deleted$')
		commands = (logline.search(l) for l in loghost.open_logfile(logfile) if ' INFO ' in l)
		commands = ((get_time(l.group('time')), l.group('sessions')) for l in commands if l is not None)
		return commands


def count_epp_command_types(logfiles):
	command_types = [
		'<login',
		'<logout',
		'<greeting',
		'<hello',
		'<poll',
		# '<ns2:check',
		# '<ns3:update',
		# '<finance:info',
		'health-check',
		'unknown',
		'<domain:info',
		'<domain:check',
		'<domain:create',
		'<domain:update',
		'<domain:renew',
		'<domain:transfer',
		'<domain:delete',
		'<contact:info',
		'<contact:check',
		'<contact:create',
		'<contact:update',
		'<contact:delete',
		'<host:info',
		'<host:check',
		'<host:create',
		'<host:update',
		'<host:delete',
	]

	command_counts = loghost.get_counts_from_logfiles(logfiles, EppCommandTypeParser(), loghost.classify_commands)

	total_command_counts = {'total': defaultdict(int)}
	for k,v in command_counts.items():
		for h,c in v.items():
			total_command_counts['total'][h] += c

	total_command_counts['health-check'] = command_counts['health-check']
	del command_counts['health-check']

	grouped = [
		{k:v for k, v in command_counts.items() if any(c in k for c in query_commands)},
		{k:v for k, v in command_counts.items() if any(c in k for c in transform_commands)},
		{k:v for k, v in command_counts.items() if not any(c in k for c in query_commands + transform_commands)},
		total_command_counts,
	]
	return grouped


def count_epp_directions(logfiles):
	command_types = [
		'NEW',
		'IN',
		'OUT',
		'END',
		'TIMEOUT',
		'DENIED',
		'IP_DENIED',
		'EXCEPTION',
		'BAD',
		'BLE',
		'IOERROR',
		'CRL',
		'ABUSE',
		'IP_WHITELIST',
		'HANDSHAKE',
		'REGISTRY_PROPERTIES',
	]

	command_counts = loghost.get_counts_from_logfiles(logfiles, EppDirectionParser(), loghost.classify_commands)

	common = ['NEW', 'END', 'IN', 'OUT']
	grouped = [
		{k:v for k,v in command_counts.items() if any(c == k for c in common)},
		{k:v for k,v in command_counts.items() if not any(c == k for c in common)},
	]
	return grouped


def main_plot_epp_traffic(env, days):
	title = 'EPP Traffic'
	print(f'Starting {title}')
	logfiles = []
	for day in days:
		logfiles = logfiles + loghost.get_logfiles(env, 'app-epp', day)

	grouped = []
	grouped += count_epp_command_types(logfiles)
	grouped += count_epp_directions(logfiles)

	plot.plot_commands_by_type(title, grouped, f'/tmp/{days}-epp-traffic.png')
	print(f'Done {title}')


def main_plot_session_limit_ips(env, days):
	title = 'Session limit exceeded IPs'
	print(f'Starting {title}')
	command_types = None

	logfiles = []
	for day in days:
		logfiles = logfiles + loghost.get_logfiles(env, 'app-epp', day)

	command_counts = loghost.get_counts_from_logfiles(logfiles, EppSessionLimitIpsParser(), loghost.classify_commands)
	for ip, c in command_counts.items():
		for h, hc in c.items():
			if h.hour == 12 and hc > 1000:
				print(ip)

	plot.plot_commands_by_type(title, [command_counts], f'/tmp/{days}-session-limited-ips.png')
	print(f'Done {title}')


def main_plot_exception_types(env, days):
	title = 'EPP exceptions'
	print(f'Starting {title}')
	command_types = [
		'java.lang.NullPointerException',
		'java.io.IOException: Too many open files',
		'java.lang.OutOfMemoryError: unable to create new native thread',
		'java.lang.InternalError: java.io.FileNotFoundException:',
		'java.net.SocketException: Too many open files (Accept failed)',
		"java.lang.RuntimeException: Can't shut down gracefully",
		'unknown',
	]

	logfiles = []
	for day in days:
		logfiles = logfiles + loghost.get_logfiles(env, 'app-epp_console', day)

	command_counts = loghost.get_counts_from_logfiles(logfiles, EppExceptionParser(), loghost.classify_commands)

	plot.plot_commands_by_type(title, [command_counts], f'/tmp/{days}-epp-exceptions.png')
	print(f'Done {title}')


def main_plot_sessions_to_terminate(env, days):
	title = 'Sessions (gtdeppxlp)'
	print(f'Starting {title}')
	command_types = ['sent']	# Matches sum_commands/newest_commands

	mw_logfiles = []
	for day in days:
		mw_logfiles = mw_logfiles + loghost.get_logfiles(env, 'gtld-middleware', day)
		# RHEL6
		# mw_logfiles = mw_logfiles + loghost.get_logfiles(day, server, 'app-middleware')
	epp_logfiles = []
	for day in days:
		epp_logfiles = epp_logfiles + loghost.get_logfiles(env, 'app-epp', day)

	# mw_to_term = loghost.get_counts_from_logfiles(mw_logfiles, MwSessionsToTerminateParser(epp_instance), loghost.sum_commands)
	mw_terminated = loghost.get_counts_from_logfiles(mw_logfiles, MwSessionsTerminatedParser(epp_instance), loghost.sum_commands)
	epp_to_term = loghost.get_counts_from_logfiles(epp_logfiles, EppSessionsToTerminateParser(), loghost.sum_commands)
	epp_total_sessions = loghost.get_counts_from_logfiles(epp_logfiles, EppTotalSessionsParser(), loghost.newest_commands)
	epp_loggedin_sessions = loghost.get_counts_from_logfiles(epp_logfiles, EppLoggedInSessionsParser(), loghost.newest_commands)

	counts = [
		{
			'To terminate (EPP)': epp_to_term['sent'],
			'Total sessions (EPP)': epp_total_sessions['sent'],
		},
		{
			'Logged in sessions (EPP)': epp_loggedin_sessions['sent'],
			'Terminated (MW)': mw_terminated['sent'],
		},
	]
	plot.plot_commands_by_type(title, counts, f'/tmp/{days}-gtdeppxlp{epp_instance}-mw-sessions.png')
	print(f'Done {title}')


if __name__== "__main__":
	start = datetime.date(2022, 9, 13)
	days = [start + datetime.timedelta(days = x) for x in range(5)]
	environment = loghost.Environment(loghost.Platform.GTLD, loghost.Deployment.REGISTRY, loghost.Lifecycle.PRODUCTION)

	# First, fetch all logfiles
	for d in days:
		loghost.get_logfiles(environment, 'app-epp', d)
		loghost.get_logfiles(environment, 'app-epp_console', d)

	main_plot_epp_traffic(environment, days)
	main_plot_exception_types(environment, days)

	# main_plot_session_limit_ips(server, days)
