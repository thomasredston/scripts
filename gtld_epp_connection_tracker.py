#!/usr/bin/env python3
# Track EPP connection activity for a registrar

import re
from collections import defaultdict
import os
import datetime
import loghost
import plot
import multiprocessing


def parse_channels(logfile, clid):
	'''
	Return dict of channel_id -> log lines
	'''
	logline = re.compile(loghost.gtld_epp_logline_base + r'(?P<type>[A-Z]+) \(\/[a-z0-9.:]+, (?P<clid>[A-Za-z0-9-]+), Y, (?P<channel>[0-9-]+)-[0-9]+\)(?P<message>.*)$')
	logincommand = re.compile(loghost.gtld_epp_logline_base + r'IN \(\/[a-z0-9.:]+, <unknown>, N, (?P<channel>[0-9-]+)-0\)(?P<message>.*)$')

	print(f"Parsing {logfile}... ")
	channels = defaultdict(list)
	commands = (logline.search(l) for l in loghost.open_logfile(logfile))
	commands = (c for c in commands if c is not None)
	loggedin_commands = (c for c in commands if c.group('clid') == clid)
	for c in loggedin_commands:
		channels[c.group('channel')].append((c.group('time'), c.group('type'), c.group('message')))

	# Login commands don't have a clid yet, so go back and match up with found channels
	print(f"Reparsing {logfile}... ")
	commands = (logincommand.search(l) for l in loghost.open_logfile(logfile))
	commands = (c for c in commands if c is not None)
	login_commands = (c for c in commands if c.group('channel') in channels)
	for c in login_commands:
		channels[c.group('channel')].insert(0, (c.group('time'), 'IN', c.group('message')))
	print(f'Found {len(channels)} channels in {logfile}')
	return channels


def to_date_string(time, year=2021):
	"""
	Time down to seconds
	"""
	return f'{year}{loghost.months[time[0:3]]}{int(time[4:6])}T{int(time[7:9])}{int(time[10:12])}{int(time[13:15])}'


def write_channels(channels, outputdir):
	if len(channels) > 0:
		try:
			os.makedirs(outputdir)
		except:
			# TODO prompt to overwrite/delete
			pass

	for channel_id, commands in channels.items():
		output = f'{outputdir}/{to_date_string(commands[0][0])}-{to_date_string(commands[-1][0])} {channel_id}.log'
		print(f"Writing {output}...")
		with open(output, 'w') as f:
			command_count = len([1 for n in commands if n[1] == 'IN'])
			f.write(f'channel {channel_id} received {command_count} commands\n\n')
			for m in commands:
				f.write(f'{m[0]} {m[1]} {m[2]}\n')


def get_time(time):
	"""
	Turn logline time (eg May 27 00:59:59.810) as date with 1hr resolution.
	"""
	# TODO year
	return datetime.datetime(2021, loghost.months[time[0:3]], int(time[4:6]), int(time[7:9]))


plot_queue = multiprocessing.Queue()
def main_connection_tracker(logfiles, clid):
	outputdir = f'/tmp/epp/{clid}'

	r = []
	with multiprocessing.Pool() as pool:
		r = pool.starmap(parse_channels, [(l, clid) for l in logfiles])

	activity = defaultdict(lambda: defaultdict(int))
	for channels in r:
		write_channels(channels, outputdir)

		for channel_id, commands in channels.items():
			for t in (get_time(c[0]) for c in commands if c[1] == 'IN'):
				activity[channel_id][t] += 1
	print(f'Found {len(activity)} channels for clid {clid}')
	plot_queue.put((f'Activity for clid {clid}', [loghost.flatten_defaultdict(activity)], f'{outputdir}/activity.png'))


if __name__== "__main__":
	environment = loghost.Environment(loghost.Platform.GTLD, loghost.Deployment.OTE, loghost.Lifecycle.PRODUCTION)

	epp_logs = loghost.get_logfiles(environment, 'app-epp', datetime.date(2022, 8, 3))

	clids = ['x7064']

	w = []
	for clid in clids:
		w.append(multiprocessing.Process(target=main_connection_tracker, args=(epp_logs, clid)))

	plot.plot_processes(w, plot_queue)
