#!/usr/bin/env bash

# export VERSION="1.3.4"

# Exit on error
set -e
# Print command
# set -x

ROOT=/git
TARGET=Release

if true; then
    pushd $ROOT/dev/registry/library/lib-common/lib-common-dnstap-sep-json
        source build/CMAKE_ENV.sh
        mkdir -p $TARGET
        pushd $TARGET
            cmake \
                -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR" \
                -DCMAKE_PREFIX_PATH="$INSTALL_DIR" \
                -DCMAKE_BUILD_TYPE=$TARGET \
                ..

            make -j 2
            # C++ tests
            pushd ../
                if [ $TARGET == "Release" ]; then
                    valgrind --tool=memcheck --leak-check=full ./$TARGET/test-dnstap-sep-json
                else
                    ./$TARGET/test-dnstap-sep-json
                fi
            popd

            # Java tests
            if false; then
                pushd ../
                    mvn test -Dtest=DnstapAsJsonIntegrationTest.java
                popd
            fi

            make -j 2 install
        popd
    popd
fi

if true; then
    pushd $ROOT/dev/registry/library/lib-common/lib-common-aws-kinesis-producer-library
        source build/CMAKE_ENV.sh
        mkdir -p $TARGET
        pushd $TARGET
            cmake \
                -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR" \
                -DCMAKE_PREFIX_PATH="$INSTALL_DIR" \
                -DCMAKE_BUILD_TYPE=$TARGET \
                ..

            make -j 2 kpl

            # C++ tests
            make -j 2 tests
            if true; then
                pushd ../
                    ./$TARGET/tests
                popd
            fi

            # Package KPL jar
            make -j 2 kinesis_producer
            if false; then
                pushd ../
                    mkdir -p java/amazon-kinesis-producer/src/main/resources/amazon-kinesis-producer-native-binaries/linux
                    cp $TARGET/kinesis_producer java/amazon-kinesis-producer/src/main/resources/amazon-kinesis-producer-native-binaries/linux
                    # mvn clean install -DskipTests -f java/amazon-kinesis-producer/pom.xml
                popd
            fi

            make -j 2 install
        popd
    popd
fi

# DNSTAP Streamer
if true; then
    pushd $ROOT/dev/registry/library/lib-service/lib-service-sep-dnstap-streamer
        source build/CMAKE_ENV.sh
        mkdir -p $TARGET
        pushd $TARGET
            cmake \
                -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR" \
                -DCMAKE_PREFIX_PATH="$INSTALL_DIR" \
                -DCMAKE_BUILD_TYPE=$TARGET \
                ..

            make -j 2 tests
            pushd ../
                ./$TARGET/tests
            popd

            make -j 2 service-sep-dnstap-streamer

            if false; then
                make package
            fi
        popd
    popd

    if false; then
        pushd $ROOT/dev/registry
            mvn clean test --projects :lib-service-sep-dnstap-streamer -am -Dtest=Streamer*IntegrationTest -DfailIfNoTests=false -T2
        #    export AWS_CBOR_DISABLE=1
        #    mvn clean test --projects :lib-service-sep-dnstap-streamer -am -Dtest=StreamerPerformanceTest.java -DfailIfNoTests=false -Dcom.amazonaws.sdk.disableCertChecking
        #    rm -f library/lib-service/lib-service-sep-dnstap-streamer/*.gnuplot library/lib-service/lib-service-sep-dnstap-streamer/*.html
        popd
    fi
fi
