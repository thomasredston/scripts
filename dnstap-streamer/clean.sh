#!/usr/bin/env bash

ROOT=/git

pushd $ROOT/dev/registry/library/lib-common/lib-common-dnstap-sep-json/build
source RUN_CLEAN.sh
popd
rm -rf $ROOT/dev/registry/library/lib-common/lib-common-dnstap-sep-json/Release $ROOT/dev/registry/library/lib-common/lib-common-dnstap-sep-json/Debug

pushd $ROOT/dev/registry/library/lib-common/lib-common-aws-kinesis-producer-library/build
source RUN_CLEAN.sh
popd
rm -rf $ROOT/dev/registry/library/lib-common/lib-common-aws-kinesis-producer-library/Release $ROOT/dev/registry/library/lib-common/lib-common-aws-kinesis-producer-library/Debug

pushd $ROOT/dev/registry/library/lib-service/lib-service-sep-dnstap-streamer/build
source RUN_CLEAN.sh
popd
rm -rf $ROOT/dev/registry/library/lib-service/lib-service-sep-dnstap-streamer/Release $ROOT/dev/registry/library/lib-service/lib-service-sep-dnstap-streamer/Debug
