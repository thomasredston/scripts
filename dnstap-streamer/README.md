# build KPL binary
* install
  * cmake >=3.5
  * [devtoolset-7](https://www.softwarecollections.org/en/scls/rhscl/devtoolset-7/)
    * `sudo yum install centos-release-scl`
    * `sudo yum install devtoolset-7 devtoolset-7-libasan-devel`
    * `scl enable devtoolset-7 bash`
  * libuuid-devel
* `./bootstrap.sh`
* `cmake`
* `make`

# Publish KPL jar
* cp kinesis_producer java/amazon-kinesis-producer/src/main/resources/amazon-kinesis-producer-native-binaries/linux
* mvn clean deploy -DskipTests

# local dev
* kpl
  * `mkfifo /tmp/amz-aws-kpl-in-pipe /tmp/amz-aws-kpl-out-pipe`
  * create config and credentials
    * `protoc --encode=aws.kinesis.protobuf.Message -I. messages.proto < configuration.txt | xxd -p | tr -d '\n'`
  * `export CA_DIR=/tmp/amazon-kinesis-producer-native-binaries/cacerts`
  * `./kinesis_producer -i /tmp/amz-aws-kpl-in-pipe -o /tmp/amz-aws-kpl-out-pipe -c 0800328F01080110FFFFFFFF0F18809003220028BB0330F403388080C00240F02E480050005A0060BB036A04696E666F70187A05736861726482010864657461696C65648A01164B696E6573697350726F64756365724C6962726172799001E0D403980101A0019601A80164B001B0EA01BA010965752D776573742D31C001F02EC80101D20100D801903FE20100EA0100F00100 -k 08FFFFFFFFFFFFFFFF7F4A44080012400A14414B494149364A565043424D544650484247475112286E66794453436C2F426F4F6C4E623345532F7553484463774E6851322B61716B6D2F315657756553 -t -w 08FFFFFFFFFFFFFFFF7F4A44080112400A14414B494149364A565043424D544650484247475112286E66794453436C2F426F4F6C4E623345532F7553484463774E6851322B61716B6D2F315657756553`
* pipeline
  * ```bash
    sudo fstrm_capture -t protobuf:dnstap.Dnstap -u /var/run/bind/dnstap.sock -w - | \
    ./git/dnstap/dnstap-ldns/dnstap-ldns -r - | \
    xargs -I {} ./git/amazon-kinesis-producer/amazon-kinesis-producer/aws/kinesis/protobuf/toMessage.sh {} > /tmp/amz-aws-kpl-in-pipe
    ```
* bind
  * requirements
    * protobuf-c
      * `export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig`
  * `sudo runuser -g named -c './named -g'`
  * `dig bbc.co.uk @localhost`
* streamer
  * requirements
    * protobuf 2.6.1
    * fstrm
    * boost >=1.64
  * `export LD_LIBRARY_PATH=/usr/local/lib`
  * `sudo ./streamer -t protobuf:dnstap.Dnstap -u /var/run/bind/dnstap.sock`
