#!/usr/bin/env bash

set -e
set -x

export LD_LIBRARY_PATH="/usr/local/lib"

# valgrind --leak-check=yes --track-origins=yes ./Debug/service-sep-dnstap-streamer &
# valgrind --tool=helgrind ./Debug/service-sep-dnstap-streamer &
# valgrind --tool=callgrind --separate-threads=yes ./Release/service-sep-dnstap-streamer &
./Release/service-sep-dnstap-streamer &

/home/thomasr/git/amazon-kinesis-producer/bind-9.12.3-P1/bin/named/named &

# resperf-report -d /usr/share/dnsperf/queryfile-example-current -c 600 -m 15000
resperf-report -d /usr/share/dnsperf/queryfile-example-current -c 60 -m 20000
# dnsperf -d /usr/share/dnsperf/queryfile-example-current -q 4000 -Q 6000 &> /dev/null

killall service-sep-dnstap-streamer named
