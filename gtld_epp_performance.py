#!/usr/bin/env python3

import os
import datetime
import loghost
from collections import defaultdict
import multiprocessing
import re
import pickle
import plot
from dataclasses import dataclass


output_dir = '/tmp/gtld-epp-performance'


def bin_date(date: datetime.datetime):
	'''
	With 1hr resolution
	'''
	return date.replace(minute=0, second=0, microsecond=0)

@dataclass
class EppCommand:
	duration: int
	command: str
	svtrid: str

def parse_epp_commands(logfile: str, operations: list[str]) -> dict[str, dict[datetime.datetime, list[EppCommand]]]:
	'''
	Get all command-response durations
	return list of duration-message tuples for each hour for each op
	'''
	print(f"Parsing {logfile}... ")
	logline = re.compile(loghost.gtld_epp_logline_base + r'(?P<type>[A-Z]+) \(\/[a-z0-9.:]+, (?:[A-Za-z0-9-]+|<unknown>), (?:Y|N), (?P<channel>[0-9-]+)-[0-9]+\) : (?P<message>.*)$')
	svtrid_finder = re.compile(r'<svTRID>(?P<svtrid>\d*)</svTRID>')
	commands = (logline.search(l) for l in loghost.open_logfile(logfile))
	commands = (c for c in commands if c is not None)
	channels_in: dict[tuple[datetime.datetime, str]] = {}
	durations = {op:defaultdict(list) for op in operations}
	for c in commands:
		id = c.group('channel')
		type = c.group('type')
		if type == 'IN':
			channels_in[id] = (c.group('time'), c.group('message'))
		elif type == 'OUT' and id in channels_in:
			# Is a response
			start_time = parse_time(channels_in[id][0])
			duration = (parse_time(c.group('time')) - start_time).total_seconds() * 1000
			in_message = channels_in[id][1]
			svtrid = svtrid_finder.search(c.group('message'))
			for op in operations:
				if op in in_message:
					durations[op][bin_date(start_time)].append(EppCommand(duration, in_message, svtrid.group('svtrid') if svtrid is not None else None))
					break
			del(channels_in[id])
	return durations


def parse_mw_commands(logfile: str, svtrids: list[str]) -> dict[str, list[tuple[datetime.datetime, str]]]:
	'''
	Get all MW command-response durations
	'''
	print(f"Parsing {logfile}... ")
	logline = re.compile(loghost.time + r' \[[0-9.]+\] \[GTLD=[a-z0-9-]*\]\[(?P<thread>[A-Za-z0-9-]+)\] INFO  (?P<message>.*)$')
	lines = (logline.search(l) for l in loghost.open_logfile(logfile))
	lines = (l for l in lines if l is not None)
	logline_bookend = re.compile(r'^u\.n\.g\.m\.s\.controller\.EppController - (?P<svtrid>\d+) - (?P<message>.*)$')
	threads = defaultdict(list)
	sessions = defaultdict(list)
	for l in lines:
		thread = l.group('thread')
		bookend_line = logline_bookend.search(l.group('message'))
		if bookend_line is not None:
			if thread not in threads:
				svtrid = bookend_line.group('svtrid')
				if svtrid in svtrids:
					threads[thread].append(svtrid)
					threads[thread].append((l.group('time'), l.group('message')))
			else:
				threads[thread].append((l.group('time'), l.group('message')))
				sessions[threads[thread][0]] = threads[thread][1:]
				del(threads[thread])
		else:
			if thread in threads:
				threads[thread].append((l.group('time'), l.group('message')))

	print(f'Matched {len(sessions)} in {logfile}')
	return sessions


def parse_time(time):
	return datetime.datetime(2021, loghost.months[time[0:3]], int(time[4:6]), int(time[7:9]), int(time[10:12]), int(time[13:15]), int(time[16:19]) * 1000)


def calculate_durations_in_logfile(logfile: str, operations: list[str]):
	base = os.path.basename(logfile)
	cachefile = f'{output_dir}/cache-{base}-gtld-epp-durations.p'
	if os.path.exists(cachefile):
		print(f'Loading cache {cachefile}...')
		return pickle.load(open(cachefile, 'rb'))

	durations = parse_epp_commands(logfile, operations)

	print('Filtering durations of fee:check...')
	if 'domain:check' in durations:
		durations['domain:check (fee)'] = defaultdict(list)
		durations['domain:check (no fee)'] = defaultdict(list)
		for h,v in durations['domain:check'].items():
			for d in v:
				if '<fee:check' in d.command:
					durations['domain:check (fee)'][h].append(d)
				else:
					durations['domain:check (no fee)'][h].append(d)
	del(durations['domain:check'])

	print('Filtering durations of fee:create...')
	if 'domain:create' in durations:
		durations['domain:create (fee)'] = defaultdict(list)
		durations['domain:create (no fee)'] = defaultdict(list)
		for h,v in durations['domain:create'].items():
			for d in v:
				if '<fee:create' in d.command:
					durations['domain:create (fee)'][h].append(d)
				else:
					durations['domain:create (no fee)'][h].append(d)
	del(durations['domain:create'])

	print('Filtering durations done')
	pickle.dump(durations, open(cachefile, 'wb'))

	return durations


def print_raw_stats(durations, cutoff, query_commands, transform_commands):
	slow_query = 0
	total_query = 0
	slow_transform = 0
	total_transform = 0
	print(f'Operations >{cutoff}ms')
	for op,v in sorted(durations.items()):
		slow = len([d for d in v if d > cutoff])
		total = len(v)
		pc = ((slow/total) * 100) if total > 0 else 0
		print(f'{op}: {slow}/{total} {pc:.2f}%')
		if op in query_commands:
			slow_query += slow
			total_query += total
		if op in transform_commands:
			slow_transform += slow
			total_transform += total
	pc = ((slow_query/total_query) * 100) if total_query > 0 else 0
	print(f'Query: {slow_query}/{total_query} {pc:.2f}%')
	pc = ((slow_transform/total_transform) * 100) if total_transform > 0 else 0
	print(f'Transform: {slow_transform}/{total_transform} {pc:.2f}%')


def get_epp_durations(operations: list, logfiles: list) -> dict[str, dict[datetime.datetime, list[EppCommand]]]:
	durations = defaultdict(lambda: defaultdict(list))
	with multiprocessing.Pool(4) as pool:
		results = pool.starmap(calculate_durations_in_logfile, [(l, operations) for l in logfiles])
		print('Merging...')
		for r in results:
			for op,v in r.items():
				for h,d in v.items():
					durations[op][h] += d

	return durations


def plot_slow_commands_over_time(durations, logfiles):
	slow_durations = defaultdict(lambda: defaultdict(int))
	total_slow_durations = {'queries': defaultdict(int), 'transforms': defaultdict(int), 'total': defaultdict(int)}
	for op,v in durations.items():
		for h,d in v.items():
			slow = sum([1 for i in d if i > 300])
			slow_durations[op][h] = slow
			total_slow_durations['total'][h] += slow
			if op in loghost.gtld_epp_query_commands:
				total_slow_durations['queries'][h] += slow
			elif op in loghost.gtld_epp_transform_commands:
				total_slow_durations['transforms'][h] += slow


	grouping = []
	for o in loghost.gtld_epp_object_types:
		grouping.append([f'{o}:{t}' for t in loghost.gtld_epp_query_types])
		grouping.append([f'{o}:{t}' for t in loghost.gtld_epp_transform_types])
	grouped = [{k:v for k, v in slow_durations.items() if any(c in k for c in group)} for group in grouping]
	grouped += [
		{k:v for k, v in slow_durations.items() if not any(c in k for c in [command for group in grouping for command in group])},
		total_slow_durations,
	]

	days = sorted(set(loghost.get_date_from_logfile(l) for l in logfiles))
	plot.plot_commands_by_type('Slow gTLD EPP Traffic', grouped, f'{output_dir}/{days[0]}-{days[-1]}-gtld-epp-slow-commands.png', datetime.timedelta(hours=1))


def write_epp_stats(durations: dict[str, dict[datetime.datetime, list[EppCommand]]], logfiles):
	raw_durations = defaultdict(list)
	for op,v in durations.items():
		for h,d in v.items():
			raw_durations[op] += [d.duration for d in d]

	# Nominet 99.5% <300ms
	print_raw_stats(raw_durations, 300, loghost.query_commands, loghost.transform_commands)

	plot_slow_commands_over_time(raw_durations, logfiles)


def get_mw_durations(slow_svtrids: list[str], mw_logfiles):
	merged = defaultdict(list)
	with multiprocessing.Pool() as pool:
		results = pool.starmap(parse_mw_commands, [(l, slow_svtrids) for l in mw_logfiles])
		print('Merging...')
		for r in results:
			for svtrid,logs in r.items():
				merged[svtrid] = logs
				break
	return merged


def write_mw_stats(merged: dict[str, list[tuple[datetime.datetime, str]]]):
	print('Processing MW stats')
	# misc_slow_lines = []
	# for op,v in merged.items():
	# 	slow_fee = []
	# 	slow_token = []
	# 	for l in v:
	# 		mw_lines = l[2]
	# 		for i,line in enumerate(mw_lines):
	# 			if (i + 1) < len(mw_lines):
	# 				next_line = mw_lines[i + 1]
	# 				duration = (parse_time(next_line[0]) - parse_time(line[0])).total_seconds() * 1000
	# 				if duration > 100:
	# 					if 'Calling the Fees Service now directly' in line[1]:
	# 						slow_fee.append(duration)
	# 					elif 'Call service-allocationToken' in line[1]:
	# 						slow_token.append(duration)
	# 					if len(line[1]) > 0:
	# 						misc_slow_lines.append((op, line[0], line[1]))
	# 						misc_slow_lines.append((op, next_line[0], next_line[1]))
	# 						misc_slow_lines.append(('','',''))
	# 	if len(slow_fee) > 0:
	# 		print(f'{op} {len(slow_fee)}/{len(v)} due to service-fee call')
	# 		plot.plot_durations(days, {'domain:check (fee)': slow_fee}, f'{output_dir}/{days[0]}-{days[-1]}-durations.png')
	# 	if len(slow_token) > 0:
	# 		print(f'{op} {len(slow_token)}/{len(v)} due to service-token call')

	with open(f'{output_dir}/mw-misc-slow.log', 'w') as f:
		for _, lines in merged.items():
			f.writelines([f'{l[0]} {l[1]}\n' for l in lines])
			f.write('\n')


def match_with_mw(durations: dict[str, dict[datetime.datetime, list[EppCommand]]], mw_logs: list[str]):
	slow_commands: list[str] = []
	for _, items in durations['domain:update'].items():
		slow_commands += [item.svtrid for item in items if item.duration > 300]

	print(f'Attempting to match {len(slow_commands)} slow commands')
	merged = get_mw_durations(slow_commands, mw_logs)
	write_mw_stats(merged)


def write_fee_stats(durations):
	fees_root = '/tmp/fees'
	if not os.path.exists(fees_root):
		os.makedirs(fees_root)

	# Filter out slow and fast durations and domains
	slow_durations = []
	fast_durations = []
	slow_domains = defaultdict(int)
	fast_domains = defaultdict(int)
	domain_finder = re.compile(r'<domain:name>(?P<name>.*)</domain:name>')
	for _,v in durations['domain:check (fee)'].items():
		slow_durations += [d for d in v if d[0] > 300]
		fast_durations += [d for d in v if d[0] <= 300]
		for d in v:
			domain = domain_finder.search(d[1]).group('name')
			if d[0] > 300:
				slow_domains[domain] += 1
			else:
				fast_domains[domain] += 1

	with open(fees_root + '/slow_domains.log', 'w') as fh:
		[fh.write(f'{d} {i}\n') for d,i in slow_domains.items() if i > 1]
	with open(fees_root + '/fast_domains.log', 'w') as fh:
		[fh.write(f'{d} {i}\n') for d,i in fast_domains.items() if i > 1]

	# slow_unique = sum([i for _,i in slow_domains.items() if i == 1])
	# slow_all = sum([i for _,i in slow_domains.items()])
	# print(f'{slow_unique} unique of {slow_all} checks')
	# fast_unique = sum([i for _,i in fast_domains.items() if i == 1])
	# fast_all = sum([i for _,i in fast_domains.items()])
	# print(f'{fast_unique} unique of {fast_all} checks')

	slow_only = set(slow_domains.keys()).difference(fast_domains.keys())
	print(f'{len(slow_only)} are only slow')

	with open(fees_root + '/slow_only.log', 'w') as fh:
		[fh.write(f'{d}\n') for d in slow_only]

	# with open(fees_root + '/shared.log', 'w') as fh:
	# 	for s in set(slow_domains.keys()).intersection(fast_domains.keys()):
	# 		[fh.write(f'{d[1]}\n') for d in slow_durations if s in d[1]]
	# 		fh.write('\n')
	# 		[fh.write(f'{d[1]}\n') for d in fast_durations if s in d[1]]
	# 		fh.write('\n\n\n')

	slow_create = sum([d[1].count('<fee:command>create') for d in slow_durations])
	slow_renew = sum([d[1].count('<fee:command>renew') for d in slow_durations])
	slow_restore = sum([d[1].count('<fee:command>restore') for d in slow_durations])
	slow_transfer = sum([d[1].count('<fee:command>transfer') for d in slow_durations])
	print(f'{slow_create}/{len(slow_durations)} ({slow_create/len(slow_durations)})')
	print(f'{slow_renew}/{len(slow_durations)} ({slow_renew/len(slow_durations)})')
	print(f'{slow_restore}/{len(slow_durations)} ({slow_restore/len(slow_durations)})')
	print(f'{slow_transfer}/{len(slow_durations)} ({slow_transfer/len(slow_durations)})')

	fast_create = sum([d[1].count('<fee:command>create') for d in fast_durations])
	fast_renew = sum([d[1].count('<fee:command>renew') for d in fast_durations])
	fast_restore = sum([d[1].count('<fee:command>restore') for d in fast_durations])
	fast_transfer = sum([d[1].count('<fee:command>transfer') for d in fast_durations])
	print(f'{fast_create}/{len(fast_durations)} ({fast_create/len(fast_durations)})')
	print(f'{fast_renew}/{len(fast_durations)} ({fast_renew/len(fast_durations)})')
	print(f'{fast_restore}/{len(fast_durations)} ({fast_restore/len(fast_durations)})')
	print(f'{fast_transfer}/{len(fast_durations)} ({fast_transfer/len(fast_durations)})')


def main_graph_epp_performance(epp_logs, mw_logs):
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)

	operations: list[str] = loghost.query_commands + loghost.transform_commands
	durations = get_epp_durations(operations, epp_logs)
	write_epp_stats(durations, epp_logs)

	# match_with_mw(durations, mw_logs)

	# write_fee_stats(durations)


if __name__== "__main__":
	start = datetime.date(2024, 4, 1)
	days = [start + datetime.timedelta(days = x) for x in range(30)]
	environment = loghost.Environment(loghost.Platform.GTLD, loghost.Deployment.REGISTRY, loghost.Lifecycle.PRODUCTION)

	epp_logs = []
	mw_logs = []
	for day in days:
		epp_logs += loghost.get_logfiles(environment, 'app-epp', day)
		# mw_logs += loghost.get_logfiles(environment, 'gtld-middleware', day)

	main_graph_epp_performance(epp_logs, mw_logs)
