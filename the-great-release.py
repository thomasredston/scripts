#!/usr/bin/env python3
# Write datafix for MMX premium price changes

import csv
import os
import cx_Oracle
from itertools import islice
import getpass
import numpy

password = getpass.getpass()

new_tier_ids = {
('abogado', '465.00', '25.00'): 2461,
('abogado', '920.00', '25.00'): 2462,
('bayern', '800.00', '200.00'): 2489,
('bayern', '200.00', '22.00'): 2509,
('bayern', '80.00', '22.00'): 2508,
('bayern', '2000.00', '400.00'): 2470,
('beer', '2500.00', '500.00'): 2471,
('beer', '800.00', '250.00'): 2490,
('beer', '200.00', '20.00'): 2529,
('beer', '80.00', '20.00'): 2518,
('boston', '80.00', '10.00'): 2510,
('boston', '200.00', '10.00'): 2511,
('boston', '2500.00', '500.00'): 2472,
('boston', '800.00', '250.00'): 2491,
('casa', '200.00', '7.50'): 2513,
('casa', '80.00', '7.50'): 2512,
('casa', '800.00', '250.00'): 2492,
('cooking', '80.00', '20.00'): 2519,
('cooking', '200.00', '20.00'): 2530,
('fashion', '200.00', '20.00'): 2531,
('fashion', '800.00', '250.00'): 2496,
('fashion', '80.00', '20.00'): 2520,
('fishing', '200.00', '20.00'): 2532,
('fishing', '80.00', '20.00'): 2521,
('fit', '800.00', '250.00'): 2494,
('fit', '200.00', '20.00'): 2533,
('fit', '80.00', '20.00'): 2522,
('garden', '80.00', '20.00'): 2523,
('garden', '200.00', '20.00'): 2534,
('horse', '800.00', '250.00'): 2498,
('horse', '200.00', '20.00'): 2535,
('horse', '80.00', '20.00'): 2524,
('law', '330.00', '75.00'): 2463,
('luxe', '800.00', '250.00'): 2499,
('luxe', '2500.00', '500.00'): 2480,
('luxe', '80.00', '15.00'): 2514,
('luxe', '200.00', '15.00'): 2515,
('miami', '80.00', '12.00'): 2516,
('miami', '800.00', '250.00'): 2500,
('miami', '200.00', '12.00'): 2517,
('rodeo', '800.00', '250.00'): 2501,
('rodeo', '80.00', '5.00'): 2540,
('rodeo', '200.00', '5.00'): 2541,
('surf', '80.00', '20.00'): 2525,
('surf', '800.00', '250.00'): 2502,
('surf', '200.00', '20.00'): 2536,
('vip', '800.00', '250.00'): 2506,
('vip', '80.00', '11.00'): 2468,
('vip', '2500.00', '500.00'): 2488,
('vip', '200.00', '11.00'): 2469,
('vodka', '200.00', '20.00'): 2539,
('vodka', '800.00', '250.00'): 2505,
('wedding', '800.00', '250.00'): 2507,
('wedding', '80.00', '20.00'): 2527,
('work', '80.00', '6.00'): 2464,
('work', '800.00', '250.00'): 2503,
('work', '200.00', '6.00'): 2465,
('yoga', '80.00', '20.00'): 2528,
('yoga', '200.00', '20.00'): 2538,
('yoga', '800.00', '250.00'): 2504,
}

prelude = '''connect driad/&&driad_password@&&database

create or replace PROCEDURE insert_history(char_key VARCHAR2, char_suffix VARCHAR2, char_op VARCHAR2, char_changedby VARCHAR2)
IS
    num_pricing_tier_id number;
    num_registration_price number;
    num_renew_price number;
BEGIN
    select t.pricing_tier_id, price
        into num_pricing_tier_id, num_registration_price
        from pricing_tiers t
        join premium_domains d on d.pricing_tier_id=t.pricing_tier_id
        join pricing_tier_entries e on t.pricing_tier_id=e.pricing_tier_id
        join products p on e.product_id=p.product_id
        where d.key = char_key and d.suffix = char_suffix
        and life_months=12
        and charge_type_id=30;

    select price
        into num_renew_price
        from pricing_tiers t 
        join premium_domains d on d.pricing_tier_id=t.pricing_tier_id
        join pricing_tier_entries e on t.pricing_tier_id=e.pricing_tier_id
        join products p on e.product_id=p.product_id
        where d.key = char_key and d.suffix = char_suffix
        and life_months=12
        and charge_type_id=31;

    insert into premium_domain_history(id, key, suffix, pricing_tier_id, first_yr_price, subs_yr_price, operation, changed_by, changed_date)
        values (PREMIUM_DOMAIN_HISTORY_SEQ.NEXTVAL, char_key, char_suffix, num_pricing_tier_id, num_registration_price, num_renew_price, char_op, char_changedby, systimestamp);
END insert_history;
/

create or replace procedure update_premium(char_key VARCHAR2, char_suffix VARCHAR2, num_tier NUMBER, char_changedby VARCHAR2)
IS
BEGIN
    insert_history(char_key, char_suffix, 'U', char_changedby);
    update premium_domains set pricing_tier_id=num_tier where key=char_key and suffix=char_suffix;
END update_premium;
/

create or replace procedure to_standard(char_key VARCHAR2, char_suffix VARCHAR2, char_changedby VARCHAR2)
IS
BEGIN
    insert_history(char_key, char_suffix, 'D', char_changedby);
    delete from premium_domains where key=char_key and suffix=char_suffix;
END to_standard;
/


INSERT INTO DB_CHANGE_LOG(CHANGE_ID, Originator, Description)
	VALUES('aho_datafix{datafix_num}-pre.sql', 'thomasr', 'MMXs The Great Release - procedures');
'''

epliogue = ''' connect driad/&&driad_password@&&database

drop procedure to_standard;
drop procedure update_premium;
drop procedure insert_history;

INSERT INTO DB_CHANGE_LOG(CHANGE_ID, Originator, Description)
	VALUES('aho_datafix{datafix_num}-post.sql', 'thomasr', 'MMXs The Great Release - clean up');
'''

class Row:
    def __init__(self, row):
        self.key = row[0][:len(row[0]) - len(row[1]) - 1]
        # print(row[0] + ' ' + self.key)
        self.suffix = row[1]
        self.classified = row[6]
        self.new_tier = row[5]
        self.renewal_tier = row[7]

def chunk(it, size):
    it = iter(it)
    return iter(lambda: tuple(islice(it, size)), ())

def write_rollback(changes, datafix_num):
    print('Writing rollback...')
    with open(f'/Users/thomasr/git/data_fixups/aho_rollback{datafix_num}.sql', 'w') as f:
        f.write('connect driad/&&driad_password@&&database\n\nset termout off;\n\n')
        f.write(f"delete from premium_domain_history where changed_by='aho_datafix{datafix_num}.sql';\n\n")

        dsn_tns = cx_Oracle.makedsn('dc2rdbalp-scan.localnet', 1521, service_name='NOMP_STB_RO')
        conn = cx_Oracle.connect(user='THOMASR_RO', password=password, dsn=dsn_tns)
        cursor = conn.cursor()

        f.write('prompt === Rolling back Premium pricing\n')
        premium_domains = [f"('{c.key}', '{c.suffix}')" for c in changes if c.classified=='Premium']
        premium_domain_chunks = chunk(premium_domains, 999)
        for premium_domains in premium_domain_chunks:
            print('Fetching premium domain data...')
            cursor.execute(f"SELECT key, suffix, pricing_tier_id from driad.premium_domains where (key, suffix) in ({','.join(premium_domains)})")
            print('Fetching premium domain data done')
            for row in cursor:
                f.write(f"update premium_domains set pricing_tier_id={row[2]} where key='{row[0]}' and suffix='{row[1]}';\n")

        f.write('\n')
        f.write('prompt === Moving domains back to Premium\n')
        standard_domains = [f"('{c.key}', '{c.suffix}')" for c in changes if c.classified=='Standard']
        standard_domain_chunks = chunk(standard_domains, 999)
        for standard_domains in standard_domain_chunks:
            print('Fetching standard domain data...')
            cursor.execute(f"SELECT * from driad.premium_domains where (key, suffix) in ({','.join(standard_domains)})")
            print('Fetching standard domain data done')
            for row in cursor:
                if (row[4] is None):
                    f.write(f"insert into premium_domains (premium_domain_id, key, suffix, pricing_tier_id) values ({row[0]}, '{row[1]}', '{row[2]}', {row[3]});\n")
                else:
                    f.write(f"insert into premium_domains (premium_domain_id, key, suffix, pricing_tier_id, g_key_rule_id) values ({row[0]}, '{row[1]}', '{row[2]}', {row[3]}, {row[4]});\n")

        f.write(f"""INSERT INTO DB_CHANGE_LOG(CHANGE_ID, Originator, Description)
	    VALUES('aho_rollback{datafix_num}.sql', 'thomasr', 'Rollback aho_datafix{datafix_num}.sql');\n""")
    print('Writing rollback done')

def write_datafix(changes, datafix_num):
    print('Writing datafix...')
    with open(f'/Users/thomasr/git/data_fixups/aho_datafix{datafix_num}.sql', 'w') as f:
        f.write("connect driad/&&driad_password@&&database\n\nset termout off;\n\n")

        print('Updating premium tiers...')
        f.write('prompt === Updating Premium pricing\n')
        for c in [c for c in changes if c.classified=='Premium']:
            if (c.suffix, c.new_tier, c.renewal_tier) in new_tier_ids:
                new_tier_id = new_tier_ids[(c.suffix, c.new_tier, c.renewal_tier)]
                f.write(f"exec update_premium('{c.key}', '{c.suffix}', {new_tier_id}, 'aho_datafix{datafix_num}.sql');\n")
            else:
                print(f'New tier not found for {c.key}.{c.suffix}')
        print('Upating premium tiers done')

        print('Updating standard tiers...')
        f.write('prompt === Moving domains to Standard pricing\n')
        for c in [c for c in changes if c.classified=='Standard']:
            f.write(f"exec to_standard('{c.key}', '{c.suffix}', 'aho_datafix{datafix_num}.sql');\n")
        print('Updating standard tiers done')

        f.write(f"""INSERT INTO DB_CHANGE_LOG(CHANGE_ID, Originator, Description)
	    VALUES('aho_datafix{datafix_num}.sql', 'thomasr', 'MMXs The Great Release - premium pricing');\n""")
    print('Writing datafix done')


if __name__== "__main__":
    datafix_num = 3376

    changes = []
    print('Reading changes...')
    with open('/Users/thomasr/Downloads/The-Great-Release-CSV-1618776935.csv', 'r', newline='') as f:
        reader = csv.reader(f)
        next(reader, None)  # Skip header
        changes = [Row(row) for row in reader if row[1] not in ('adult', 'nrw', 'porn', 'sex', 'xxx') and row[4]!=row[5]]
        print(len(changes))
    print('Reading changes done')

    with open(f'/Users/thomasr/git/data_fixups/aho_datafix{datafix_num}-pre.sql', 'w') as f:
        f.write(prelude.format(datafix_num=datafix_num))
    with open(f'/Users/thomasr/git/data_fixups/aho_datafix{datafix_num}-post.sql', 'w') as f:
        f.write(epliogue.format(datafix_num=datafix_num))

    i = 1
    for part in numpy.array_split(changes, 5):
        write_rollback(part, f"{datafix_num}-{i}")
        write_datafix(part, f"{datafix_num}-{i}")
        i += 1
