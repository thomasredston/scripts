#!/usr/bin/env bash

protoc -I=. --cpp_out=. config.proto
cp -u config.pb.* ../../../../../lib-service/lib-service-sep-dnstap-streamer/src/protobuf/

protoc -I=. --java_out=. config.proto
cp -u com/amazonaws/services/kinesis/producer/protobuf/Config.java ../../../java/amazon-kinesis-producer/src/main/java/com/amazonaws/services/kinesis/producer/protobuf/
