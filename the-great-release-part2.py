#!/usr/bin/env python3
# Write datafix for MMX allocation token and reserved name changes

import csv
import os
import cx_Oracle
from itertools import islice
import getpass

password = getpass.getpass()

class AllocationToken:
    def __init__(self, row):
        domain = row[0].rsplit('.', 1)
        self.key = domain[0]
        self.suffix = domain[1]
        assert row[1] == 'Remove'
        self.token = row[2]
        assert row[3] == ''
        assert row[4] == ''

class ReservedName:
    def __init__(self, row):
        domain = row[0].rsplit('.', 1)
        self.key = domain[0]
        self.suffix = domain[1]
        assert row[1] == 'Remove'
        assert row[2] == 'Reserved'


def chunk(it, size):
    it = iter(it)
    return iter(lambda: tuple(islice(it, size)), ())

def write_allocation_token_rollback(allocation_token_changes, datafix_num, f):
    print('Writing rollback...')
    f.write(f"delete from domain_tokens_history where changed_by='aho_datafix{datafix_num}.sql';\n\n")

    dsn_tns = cx_Oracle.makedsn('dc2rdbalp-scan.localnet', 1521, service_name='NOMP_STB_RO')
    conn = cx_Oracle.connect(user='THOMASR_RO', password=password, dsn=dsn_tns)
    cursor = conn.cursor()

    f.write('prompt === Restoring Allocation Tokens\n')
    allocation_token_chunks = chunk([f"('{c.key}', '{c.suffix}')" for c in allocation_token_changes], 999)
    for tokens in allocation_token_chunks:
        print('Fetching data...')
        cursor.execute(f"SELECT * from dotgtldcon.domain_tokens where (key, suffix) in ({','.join(tokens)})")
        print('Fetching data done')
        for row in cursor:
            if row[4] is None:
                f.write(f"""insert into domain_tokens (id, key, suffix, token, single_use) values ({row[0]}, '{row[1]}', '{row[2]}', '{row[3]}', '{row[5]}');\n""")
            else:
                f.write(f"""insert into domain_tokens (id, key, suffix, token, expiry, single_use) values ({row[0]}, '{row[1]}', '{row[2]}', '{row[3]}', {row[4]}, '{row[5]}');\n""")
    print('Writing rollback done')

def write_allocation_token_datafix(allocation_token_changes, datafix_num, f):
    print('Writing allocation token datafix...')
    f.write('prompt === Removing Allocation Tokens\n')
    for c in allocation_token_changes:
        f.write(f"""insert into domain_tokens_history (id, key, suffix, token, operation, changed_by, changed_date, token_id, single_use)
        select DOMAIN_TOKENS_HISTORY_ID_SEQ.NEXTVAL, '{c.key}', '{c.suffix}', token, 'D', 'aho_datafix{datafix_num}.sql', sysdate, id, single_use from domain_tokens where key='{c.key}' and suffix='{c.suffix}';\n""")
        f.write(f"delete from domain_tokens where key='{c.key}' and suffix='{c.suffix}';\n")
    print('Writing allocation token datafix done')

def write_reserved_name_rollback(reserved_name_changes, datafix_num, f):
    print('Writing reserved name rollback...')
    dsn_tns = cx_Oracle.makedsn('dc2rdbalp-scan.localnet', 1521, service_name='NOMP_STB_RO')
    conn = cx_Oracle.connect(user='THOMASR_RO', password=password, dsn=dsn_tns)
    cursor = conn.cursor()

    f.write('prompt === Rolling back Reserved Names\n')
    chunks = chunk([f"('{c.key}', '{c.suffix}')" for c in reserved_name_changes], 999)
    for tokens in chunks:
        print('Fetching data...')
        cursor.execute(f"SELECT * from dotgtldcon.dot_reserved_names where (name, suffix) in ({','.join(tokens)})")
        print('Fetching data done')
        for row in cursor:
            if row[4] is None:
                f.write(f"""insert into dot_reserved_names (id, name, dot_reserved_name_type, dot_reserved_name_sub_type, suffix)
                values ({row[0]}, '{row[1]}', {row[2]}, {row[3]}, '{row[5]}');\n""")
            else:
                f.write(f"""insert into dot_reserved_names (id, name, dot_reserved_name_type, dot_reserved_name_sub_type, dot_reserved_name_response, suffix)
                values ({row[0]}, '{row[1]}', {row[2]}, {row[3]}, {row[4]}, '{row[5]}');\n""")

    f.write('prompt === Rolling back Reserved Names History\n')
    for c in reserved_name_changes:
        f.write(f"delete from dot_reserved_names_history where name='{c.key}' and suffix='{c.suffix}' and trunc(changed_date)=trunc(sysdate);\n")
        f.write(f"delete from domains_in_claims where key='{c.key}' and suffix='{c.suffix}';\n")
    print('Writing reserved name rollback done')

def write_reserved_name_datafix(reserved_name_changes, datafix_num, f):
    print('Writing reserved name datafix...')
    f.write('prompt === Releasing Reserved Names\n')
    for c in reserved_name_changes:
        f.write(f"""insert into dot_reserved_names_history (id, name, dot_reserved_name_type, dot_reserved_name_sub_type, dot_reserved_name_response, suffix, operation, changed_date)
        select dot_reserved_names_hist_seq.nextval, '{c.key}', dot_reserved_name_type, dot_reserved_name_sub_type, dot_reserved_name_response, '{c.suffix}', 'D', sysdate from dot_reserved_names where name='{c.key}' and suffix='{c.suffix}';\n""")
        f.write(f"insert into domains_in_claims (key, suffix, claims_end) values ('{c.key}', '{c.suffix}', sysdate + 90);\n")
        f.write(f"delete from dot_reserved_names where name='{c.key}' and suffix='{c.suffix}';\n")
    print('Writing reserved name datafix done')

if __name__== "__main__":
    datafix_num = 3378

    allocation_token_changes = []
    print('Reading allocation_token_changes...')
    with open('/Users/thomasr/Downloads/AllocationTokens-Remove[1].csv', 'r', newline='') as f:
        reader = csv.reader(f)
        next(reader, None)  # Skip header
        allocation_token_changes = [AllocationToken(row) for row in reader]
        print(len(allocation_token_changes))
    print('Reading allocation_token_changes done')

    reserved_name_changes = []
    print('Reading reserved_name_changes...')
    with open('/Users/thomasr/Downloads/ReservedNames-Remove[1].csv', 'r', newline='') as f:
        reader = csv.reader(f)
        next(reader, None)  # Skip header
        reserved_name_changes = [ReservedName(row) for row in reader]
        print(len(reserved_name_changes))
    print('Reading reserved_name_changes done')

    # allocation_token_changes = allocation_token_changes[:100]
    # reserved_name_changes = reserved_name_changes[:100]

    with open(f'/Users/thomasr/git/data_fixups/aho_rollback{datafix_num}.sql', 'w') as f:
        f.write('connect dotgtldcon/&&dotgtldcon_password@&&database\n\nset termout off;\n\n')
        write_allocation_token_rollback(allocation_token_changes, datafix_num, f)
        write_reserved_name_rollback(reserved_name_changes, datafix_num, f)

        f.write(f"""INSERT INTO DB_CHANGE_LOG(CHANGE_ID, Originator, Description)
        VALUES('aho_rollback{datafix_num}.sql', 'thomasr', 'Rollback aho_datafix{datafix_num}.sql');""")

    with open(f'/Users/thomasr/git/data_fixups/aho_datafix{datafix_num}.sql', 'w') as f:
        f.write('connect dotgtldcon/&&dotgtldcon_password@&&database\n\nset termout off;\n\n')
        write_allocation_token_datafix(allocation_token_changes, datafix_num, f)
        write_reserved_name_datafix(reserved_name_changes, datafix_num, f)

        f.write(f"""INSERT INTO DB_CHANGE_LOG(CHANGE_ID, Originator, Description)
        VALUES('aho_datafix{datafix_num}.sql', 'thomasr', 'The Great Release - allocation tokens and reserved names');""")
