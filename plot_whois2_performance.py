#!/usr/bin/env python3

import os
import datetime
import matplotlib.pyplot as plt
import numpy
import loghost
from collections import defaultdict
import multiprocessing
import re


def parse_commands(logfile):
	'''
	Get all durations
	'''
	print(f"Parsing {logfile}... ")
	logline = re.compile(loghost.time + r' - \[ID \d+ info\] /\S+ \S* (?P<duration>[0-9]+)\n')
	logline2 = re.compile(loghost.time + r' - \[ID \d+ info\] /\S+ \S+ \S* \S+\s+(?P<duration>[0-9]+)\n')
	commands = []
	for l in loghost.open_logfile(logfile):
		m = logline.search(l)
		if m is not None:
			commands.append(int(m.group('duration')))
		else:
			m = logline2.search(l)
			if m is not None:
				commands.append(int(m.group('duration')))
	return commands


def plot_durations(title, durations, filename, logfiles):
	print('Plotting...')
	plt.figure(figsize=[16, 9])
	files = ', '.join([os.path.basename(l) for l in logfiles])
	plt.title(f'{title}\n{files}')

	duration_min = min(durations)
	duration_max = max(durations)

	ticks = []
	bins = []
	for i in range(1, 10):
		ticks.append(i)
		ticks.append(i * 10)
		ticks.append(i * 100)
		bins.append(i)
		bins.append(i * 10)
		bins.append(i * 100)
		for j in range(1, 10):
			bins.append(i * 10 + j)
			bins.append(i * 100 + j * 10)
	ticks.sort()
	ticks = [t for t in ticks if duration_min < t and t < duration_max]
	bins.sort()
	bins = [t for t in bins if duration_min < t and t < duration_max]

	plt.hist(durations, bins=bins, alpha=0.5, density=True)

	plt.xscale('log')
	plt.xticks(ticks, ticks)
	plt.xlabel('Duration (ms)')
	plt.ylabel('%')
	plt.savefig(filename)
	print('Plotting done')


def main_graph_whois2_performance(days):
	for day in days:
		logfiles = loghost.get_logfiles(day, 'ukxwhoxlp', 'whois2_timing', ['01', '02'])
		with multiprocessing.Pool() as pool:
			results = pool.starmap(parse_commands, [(l,) for l in logfiles])

			# Merge
			durations = []
			for r in results:
				durations += r

			if len(durations) > 0:
				slow = len([d for d in durations if d > 200])
				total = len(durations)
				pc = ((slow/total) * 100)
				print(f' {slow}/{total} {pc:.1f}%')

				plot_durations(f'{day}', durations, f'/tmp/{day}-whois2-performance.png', logfiles)


if __name__== "__main__":
	days = [20210404, 20210405, 20210406, 20210416, 20210417, 20210418, 20210419, 20210420]
	main_graph_whois2_performance(days)
