#!/usr/bin/env bash

export PATH="${PATH}:/home/thomasr/git/amazon-kinesis-producer/jemalloc-5.2.0/bin"
export JAVA_HOME="/usr/lib/jvm/jre-1.8.0-openjdk.x86_64/"
export LD_LIBRARY_PATH="/usr/local/lib"

export THIRD_PARTY_LIB="/usr/local/lib"
export THIRD_PARTY_INCLUDE="/home/thomasr/git/amazon-kinesis-producer/amazon-kinesis-producer/third_party/include"
export LDNS_INCLUDE="/usr/local/include"
export A_KPL_INCLUDE="/usr/local/include"
export DNSTAP_SEP_JSON_INCLUDE="/usr/local/include"
export DNSTAP_SEP_JSON_LIB=$THIRD_PARTY_LIB
export FSTRM_INCLUDE=$LDNS_INCLUDE
export FSTRM_LIB=$THIRD_PARTY_LIB
export A_KPL_LIB=$THIRD_PARTY_LIB
export EVENT_LIB="/usr/lib64"
export BOOST_TEST_INCLUDE=$LDNS_INCLUDE
export ZMQ_LIB=$THIRD_PARTY_LIB
export JEMALLOC_LIB=$THIRD_PARTY_LIB

set -e
set -x

mkdir -p Release
pushd Release
cmake ..
make
popd

# ./Release/test-dnstap-sep-json --run_test=DnstapProtobufToJson/wins

valgrind --tool=memcheck --leak-check=full ./Release/test-dnstap-sep-json

# valgrind --tool=callgrind --dump-instr=yes --simulate-cache=yes --collect-jumps=yes ./Release/test-dnstap-sep-json --run_test=DnstapProtobufToJson/perf
# chmod 644 callgrind.out.*
