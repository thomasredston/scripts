#!/usr/bin/env bash

set -e
set -x

export LD_LIBRARY_PATH="/usr/local/lib"

fstrm_capture -u /var/run/dstreamer/dnstap.sock -t protobuf:dnstap.Dnstap -w src/test/resources/unexpected-type.fstrm &
/home/thomasr/git/amazon-kinesis-producer/bind-9.12.3-P1/bin/named/named -g &
sleep 2

dig @localhost ANY fanaptelecom.net.

sleep 10
killall named fstrm_capture
# resperf-report -d /usr/share/dnsperf/queryfile-example-current -m 1000 -r 10
