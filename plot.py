import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import time
import math
import datetime


def add_axes(axes: plt.Axes, hours: list[datetime.datetime]):
	bins = sorted(hours)
	if len(bins) > 0:
		total_delta = bins[-1] - bins[0]
		if total_delta >= datetime.timedelta(days=7):
			axes.xaxis.set_major_locator(mdates.DayLocator())
			axes.xaxis.set_minor_locator(mdates.HourLocator(byhour=range(0, 24, 6)))
		elif total_delta >= datetime.timedelta(days=1):
			axes.xaxis.set_major_locator(mdates.HourLocator(byhour=range(0, 24, 6)))
			axes.xaxis.set_minor_locator(mdates.HourLocator(byhour=range(0, 24, 1)))
		else:
			axes.xaxis.set_major_locator(mdates.HourLocator(byhour=range(0, 24, 1)))
			axes.xaxis.set_minor_locator(mdates.MinuteLocator(byminute=range(0, 60, 1)))
		axes.xaxis.set_major_formatter(mdates.DateFormatter('%b %d %I%p'))

		for label in axes.xaxis.get_ticklabels(which='major'):
			label.set(rotation=30, horizontalalignment='right')
		axes.set_ylabel('Count')
		axes.set_ylim(bottom=0)
		axes.legend(loc='best')


def subplot_commands_by_type(axes: plt.Axes, command_counts, resolution):
	# Fill in the blanks with zeros
	for command, query_times in command_counts.items():
		if len(query_times) > 0:
			given_times = sorted(query_times.keys())
			all_times = [given_times[0]]
			all_counts = [query_times[given_times[0]]]
			while all_times[-1] < given_times[-1]:
				cur = all_times[-1] + resolution
				all_times.append(cur)
				all_counts.append(query_times[cur] if cur in query_times else 0)
			axes.plot(all_times, all_counts, label=command)

	# Draw the empty figure above so we maintain order/position, but we can skip decoration
	if len(command_counts) > 0:
		add_axes(axes, list(command_counts[list(command_counts)[0]].keys()))


def plot_commands_by_type(title, grouped_counts, filename, resolution = datetime.timedelta(minutes=10)):
	print(f'Plotting {filename}...')
	if len(grouped_counts) == 0:
		return

	col = 2
	row = math.ceil(len(grouped_counts) / 2)
	if len(grouped_counts) == 1:
		col = 1
		row = 1

	figure = plt.figure(figsize=(8 * col, 6 * row))
	figure.suptitle(f'{title}')
	for i, c in enumerate(grouped_counts):
		subplot_commands_by_type(figure.add_subplot(row, col, i+1), c, resolution)
	figure.subplots_adjust(left=0.1, right=1)
	figure.savefig(filename)
	plt.close(figure)
	print(f'Plotting {filename} done')


def plot_processes(processes, plot_queue):
	for r in processes:
		r.start()
	while any([r.is_alive() for r in processes]):
		if not plot_queue.empty():
			plot = plot_queue.get()
			plot_commands_by_type(plot[0], plot[1], plot[2])
		else:
			time.sleep(1)

	for r in processes:
		r.join()

	while not plot_queue.empty():
		plot = plot_queue.get()
		plot_commands_by_type(plot[0], plot[1], plot[2])


def plot_durations(title: str, durations, filename: str):
	print(f'Plotting durations histogram {filename}...')
	dir,_ = os.path.split(filename)
	if not os.path.exists(dir):
		os.makedirs(dir)

	duration_min = 10000
	duration_max = 0
	for op, v in durations.items():
		if len(v) > 0:
			duration_min = min(duration_min, min(v))
			duration_max = max(duration_max, max(v))

	ticks = []
	bins = []
	for i in range(1, 10):
		ticks.append(i)
		ticks.append(i * 10)
		ticks.append(i * 100)
		bins.append(i)
		bins.append(i * 10)
		bins.append(i * 100)
		for j in range(1, 10):
			bins.append(i * 10 + j)
			bins.append(i * 100 + j * 10)
	ticks.sort()
	ticks = [t for t in ticks if duration_min < t and t < duration_max]
	bins.sort()
	bins = [t for t in bins if duration_min < t and t < duration_max]

	plt.figure(figsize=[16, 9])
	plt.title(f'{title}')
	for op, v in durations.items():
		if len(v) > 0 and len(bins) > 0:
			plt.hist(v, bins=bins, alpha=0.5, label=op, density=True)

			plt.xscale('log')
			plt.xticks(ticks, ticks)
			plt.xlabel('Duration (ms)')
			plt.ylabel('%')
			plt.legend(loc='best')
			plt.savefig(filename)
		else:
			print(f'Figure {op} is empty')
	plt.close()
	print('Plotting done')
